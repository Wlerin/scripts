// ==UserScript==
// @name        Youtube - Block title change
// @namespace   https://gitlab.com/loopvid/scripts
// @description Block title change
// @include     https://www.youtube.com/*
// @version     1.0
// @grant       none
// @run-at      document-start
// ==/UserScript==

(function() {
  'use strict';

  var notifications_regex = new RegExp('^\\(\\d+\\) (.*)');

  function check_title() {
    var match = notifications_regex.exec(document.title);
    if (match) {
      console.log('blocking title change, title='+document.title);
      document.title = match[1];
    }
  }

  function set_up() {
    var observer = new MutationObserver(check_title);

    // https://stackoverflow.com/a/11694229
    observer.observe(
      document.querySelector('head>title'),
      {subtree: true, characterData: true, childList: true});
  }

  document.addEventListener('DOMContentLoaded', set_up);
}());
