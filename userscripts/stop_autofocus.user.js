// ==UserScript==
// @name          Stop autofocus
// @namespace     https://gitlab.com/loopvid/scripts
// @description   Stop pages from autofocusing input field
// @include       http://*
// @include       https://*
// @version       1.0
// @grant         none
// @run-at        document-start
// ==/UserScript==

(function(){
  'use strict';

  // don't run again for each frame in page
  if (window.self !== window.top) { return; }

  var monitoring = true;

  // discover what this browser uses for visibility
  // https://developer.mozilla.org/en-US/docs/Web/Guide/User_experience/Using_the_Page_Visibility_API
  var hidden_property,
      visibility_change_event,
      visibility_api_available = true;
  if (document.hidden !== undefined) {
    hidden_property = 'hidden';
    visibility_change_event = 'visibilitychange';
  } else if (document.mozHidden !== undefined) {
    hidden_property = 'mozHidden';
    visibility_change_event = 'mozvisibilitychange';
  } else if (document.msHidden !== undefined) {
    hidden_property = 'msHidden';
    visibility_change_event = 'msvisibilitychange';
  } else if (document.webkitHidden !== undefined) {
    hidden_property = 'webkitHidden';
    visibility_change_event = 'webkitvisibilitychange';
  } else {
    visibility_api_available = false;
  }

  function block_autofocus(element) {
    if (element.tagName === 'INPUT') {
      console.log('blocking autofocus');
      element.blur();
    }
  }

  function block_focus_visible(e) {
    /* block focus while page is visible if the user hasn't inputted anything */
    if (!monitoring) { return; }
    block_autofocus(e.target);
  }

  function block_focus_not_visible(e) {
    /* always block focus when page is not visible */
    if (visibility_api_available && document[hidden_property]) {
      block_autofocus(e.target);
    }
  }

  function blur_active_element() {
    /* blur currently focused element when window get hidden */
    if (visibility_api_available && document[hidden_property]) {
      block_autofocus(document.activeElement);
    }
  }

  function stop_monitoring() {
    //console.log('stopping monitoring');
    monitoring = false;
    // remove listeners we setup
    document.removeEventListener('mousedown', stop_monitoring);
    document.removeEventListener('keydown', stop_monitoring);
    window.removeEventListener('allowFocus', stop_monitoring);
    window.removeEventListener('load', stop_monitoring_delay);
    document.removeEventListener('focus', block_focus_visible, true);
  }

  function stop_monitoring_delay() {
    window.setTimeout(stop_monitoring, 10000);
  }

  // stop monitoring on user input, so the user can focus if he wants
  document.addEventListener('mousedown', stop_monitoring);
  document.addEventListener('keydown', stop_monitoring);

  // custom event for allowing vimperator to focus
  window.addEventListener('allowFocus', stop_monitoring);

  /* stop monitoring a while after page load so that all autofocus js will have
   * already been run (hopefully) */
  window.addEventListener('load', stop_monitoring_delay);

  /* event must be captured because the inputs that will be receiving focus don't
   * exist yet (because we run at document-start) */
  document.addEventListener('focus', block_focus_visible, true);
  document.addEventListener('focus', block_focus_not_visible, true);

  /* remove focus if windows gets hidden */
  document.addEventListener(visibility_change_event, blur_active_element);

  //console.log('starting to block autofocus');
}());
