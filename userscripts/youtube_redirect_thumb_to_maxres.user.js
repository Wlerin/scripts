// ==UserScript==
// @name        Youtube - redirect to max res thumbnail
// @namespace   https://gitlab.com/loopvid/scripts
// @description Redirect lower res youtube thumbnails to max res
// @include     http://i.ytimg.com/vi/*/*default.jpg
// @include     https://i.ytimg.com/vi/*/*default.jpg
// @include     http://i.ytimg.com/vi_webp/*/*default.webp
// @include     https://i.ytimg.com/vi_webp/*/*default.webp
// @version     2.1
// @grant       none
// @run-at      document-start
// ==/UserScript==

/* The thumbnails available for a video are:
 * default        = 90p
 * mqdefault      = 240p
 * hqdefault      = 360p
 * sddefault      = 480p
 * maxresdefault  = the same res as the highest available for the video
 *                      (only available for HD video)
 *
 * This script will redirect from any of the lower resolutions to
 * maxresdefault (so you can, for example, open a thumb in a new tab and
 * inspect the maxres before deciding whether to open the video or not). */

(function() {
  'use strict';

  var url = window.location.href;

  // make sure we're at the right place
  if (!/\/([mhs][qd])?default\.(jpg|webp)$/.test(url)) { return; }

  function check_maxres() {
    /* send a HEAD to find out if maxres is available for this thumb */
    var new_url;

    new_url = url.replace(/([mhs][qd])?default\.(jpg|webp)$/, 'maxresdefault.$2');

    // just to to make sure
    if (new_url == url) { return; }

    var request = new window.XMLHttpRequest();
    request.open('HEAD', new_url);
    request.onload = function() {
      if (request.status >= 200 && request.status < 400){
        // maxres is available: redirect
        window.location.replace(new_url);
      }
    };
    request.send();
  }

  // check if maxres is available and if so, redirect
  check_maxres();

}());
