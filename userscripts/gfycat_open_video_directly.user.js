// ==UserScript==
// @name        gfycat - open video directly
// @namespace   https://gitlab.com/loopvid/scripts
// @description Avoid gfycat's interface by opening the video directly
// @include     https://gfycat.com/*
// @include     http://gfycat.com/*
// @include     https://www.gfycat.com/*
// @include     http://www.gfycat.com/*
// @version     1.0
// @grant       none
// @run-at      document-start
// ==/UserScript==

(function() {
  'use strict';

  var path = window.location.pathname;

  /* check if actual video page (that is, not their front page, the video
   * itself, etc) */
  if (!/^\/([A-Z][a-z]+){3}$/.test(path)) { return; }

  function stop_video(video) {
    video.pause();
    // remove sources
    while (video.firstChild) {
      video.removeChild(video.firstChild);
    }
    // remove video
    video.parentNode.removeChild(video);
  }

  function redirect_to_source(source) {
    console.log('redirecting to source');
    var source_url = source.src;
    stop_video(source.parentNode);
    window.location.replace(source_url);
  }

  function wait_for_source() {
    var interval, sources = document.getElementsByTagName('source');

    // try running immediately
    if (sources.length > 0) {
      redirect_to_source(sources[0]);
    }
    else {
      console.log('waiting for source');
      interval = window.setInterval(function() {
        if (sources.length > 0) {
          window.clearInterval(interval);
          redirect_to_source(sources[0]);
        }
      }, 200);
    }
  }

  wait_for_source();
}());
