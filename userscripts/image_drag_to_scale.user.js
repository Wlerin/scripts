// ==UserScript==
// @name          image files - drag to scale
// @namespace     https://gitlab.com/loopvid/scripts
// @description   Scale images by dragging; fit to window by clicking; also enable keyboard hotkeys for both actions.
// @include       *
// @version       0.9
// @grant         none
// @run-at        document-start
// ==/UserScript==

// TODO: user settings ( http://stackoverflow.com/a/9449320 )

/* This script works on a single image, that is, a tab or window showing only
 * an image, not images within a web page. This is checked every time, so it's
 * safe to run it for all pages.
 *
 * Resize the image by grabbing and dragging it with your left mouse button.
 * Dragging towards bottom-right grows the image, towards top-left shrinks
 * it.
 *
 * You can also toggle between fitting the image to the window or displaying it
 * at its original resolution by clicking on it.
 *
 * Images smaller than the window on both dimensions will default to their
 * original resolutions, while bigger images will default to fitting the window.
 *
 * The script prevents the default browser behavior from interfering, but even
 * so, firefox users may consider setting "browser.enable_click_image_resizing"
 * and "browser.enable_automatic_image_resizing" to false in about:config. */

(function () {
  'use strict';

  /* options */
  var debug = false;
  var background_color = '#181818';

  /* change to false if you don't want the mouse to resize the image by default */
  var mouse_resize_enabled = true;
  /* change either to false if you want to resize on a single axis */
  var resize_on_x_axis = true;
  var resize_on_y_axis = true;

  /* change to false if you don't want the script to capture any keyboard key
   * presses */
  var hotkeys_enabled = true;
  /* You can use either the string that represents the key, if it is a regular
   * letter, number or symbol, or the key code for everything else (note that
   * chrome will not let us use many of the special keys). Modifier keys
   * (shift, crtl, alt) are not available at the moment.
   *
   * See http://unixpapa.com/js/key.html for key codes, or turn on debugging
   * and watch the console. Some examples:
   * enter = 13
   * tab = 9
   * F1 to F12 = 112 to 123
   * left arrow = 37
   * up arrow = 38
   * right arrow = 39
   * down arrow = 40
   *
   * If you'll be adding lines here, make sure all lines except for the last
   * one end in a comma */
  var hotkeys = {
    '+': 'zoom_in',
    '-': 'zoom_out',
    '*': 'toggle_fit',
    '0': 'fit_to_window',
    '1': 'reset_zoom',
    ' ': 'disable_mouse_resize' // space
  };

  /* This controls how much the image will scale when using the hotkeys to zoom
   * in and out. Increase it if it's zooming too little, and decrease it if too
   * much. */
  var hotkey_zoom_delta = 50;

  /* The base multiplier controls how much the image should scale according to
   * how much the mouse was moved. The higher the value, the greatest the
   * impact of mouse movement on the image.
   *
   * Increase this if you think images are zooming too slowly and decrease it if
   * they're zooming too fast. */
  var base_multiplier = 1.5;
  /* The proportional multiplier controls how differently the image should
   * scale depending on how small or large it is right now. A value of zero
   * means the image will scale the same regardless of it size. A larger value
   * means the image will grow relatively faster once it becomes bigger.
   *
   * Increase this (and decrease the base multiplier) if you think the image
   * zooms too slowly once it is already big, and decrease it (and increase the
   * base multiplier) if it zooms too fast. */
  var proportional_multiplier = 1;
  /* This limits the scale rate, which is calculated with the multipliers
   * above. You should increase it if you have increased the multipliers in
   * order to get more aggressive zooming. */
  var scale_rate_limit = 10;

  /* end of options */



  /* make sure the current document is an image, otherwise don't run at all */
  // document.contentType is only available in firefox
  if (document.contentType !== undefined) {
    if (debug) { console.log('content-type: '+document.contentType); }
    if (!/^image\//.test(document.contentType)) { return; }
  }
  /* We'll also check that the document body consists of a single image, down
   * below at check_body(). This works for all browsers.
   * We can't just do it here because we're running at document-start, so we
   * have to make sure the DOM has been loaded first. */

  /* also check that we're not inside a frame */
  try {
    if (window.self !== window.top) { return; }
  }
  catch (e) { return; }

  var natural_width, natural_height;
  function setup_controls() {
    if (debug) { console.log('image is ready; setting up controls'); }
    var body = document.body;
    body.style.margin = '0';
    body.style.background = background_color;

    var images = document.getElementsByTagName("img");
    if (images.length === 0) { return; }

    // don't do anything if it seems that we're running for the second time
    if (document.getElementById('image') !== null ||
        document.getElementById('image_container') !== null ||
        document.body.firstChild.tagName !== 'IMG') {
      console.log('warning: image controls are already set up');
      return;
    }

    var image = images[0];
    image.id = 'image';
    image.className = '';
    image.style.position = 'static';
    image.style.margin = '0';

    var image_container = document.createElement('div');
    image_container.id = 'image_container';
    image_container.appendChild(image);
    image_container.style.overflow = 'hidden';
    image_container.style.position = 'relative';
    body.appendChild(image_container);

    var transforms = ["transform", "WebkitTransform", "MozTransform", "msTransform", "OTransform"];
    var i, transform;
    for (i=0; i < transforms.length; i++) {
      if(image.style[transforms[i]] !== undefined){
        transform = transforms[i];
        break;
      }
    }

    var start_scale = 0;
    var new_scale = 0;
    var fitted_to_window = false;

    var translate_x = 0;
    var translate_y = 0;

    // to be set during mouse down
    var initial_scroll_left = 0;
    var initial_scroll_top = 0;
    var scroll_left = 0;
    var scroll_top = 0;

    // higher scale_rate means faster scaling.
    // (roughly, mouse_distance_moved*scale_rate = change_in_image_dimensions)
    var scale_rate = 1;
    function set_new_scale_rate() {
      var image_diagonal = Math.sqrt(Math.pow(image.scrollWidth, 2) + Math.pow(image.scrollHeight, 2));
      var window_diagonal = Math.sqrt(Math.pow(window.innerWidth, 2) + Math.pow(window.innerHeight, 2));

      /* change the multiplier according to how big the scaled
       * image is (so that a bigger image grows faster)
       *
       * this is meant to give our eyes the impression of linear
       * growth (actual linear growth (with a fixed multiplier)
       * feels like it slows down with size (i.e. our perception
       * is logarithmic))*/
      scale_rate = base_multiplier + (image_diagonal*new_scale/window_diagonal)*proportional_multiplier;
      // don't go too crazy
      if (scale_rate > scale_rate_limit) { scale_rate = scale_rate_limit; }
    }
    set_new_scale_rate();

    var debug_div;
    if (debug) {
      debug_div = document.createElement('div');
      debug_div.style.position = 'fixed';
      debug_div.style.top = '10px';
      debug_div.style.left = '10px';
      debug_div.style.background = '#000';
      debug_div.style.color = '#fff';
      debug_div.style.zIndex = '1000';
      debug_div.style.display = 'inline';
      debug_div.id = 'debug_div';
      body.appendChild(debug_div);
      debug_div = document.getElementById('debug_div');
    }

    function update_debug_div() {
      if (debug && debug_div) {
        if (debug_div.style.display === 'none') { return; }

        var image_left = parseInt(image_container.style.left, 10);
        var image_top = parseInt(image_container.style.top, 10);

        debug_div.innerHTML = '';
        // original dimensions
        debug_div.innerHTML += image.width+','+image.height;
        // scale
        debug_div.innerHTML += '*'+new_scale.toFixed(2);
        // scale rate
        set_new_scale_rate();
        debug_div.innerHTML += '(*'+scale_rate.toFixed(2)+')';
        // scaled dimensions
        debug_div.innerHTML += ' = '+Math.round(image.width*new_scale)+','+Math.round(image.height*new_scale);
        // translation and centering
        debug_div.innerHTML += ':'+translate_x+'(';
        if (image_left >= 0) debug_div.innerHTML += '+';
        debug_div.innerHTML += image_left+')';
        debug_div.innerHTML += ','+translate_y+'(';
        if (image_top >= 0) debug_div.innerHTML += '+';
        debug_div.innerHTML += image_top+')';
        // window dimensions and (scrolling) position
        debug_div.innerHTML += ' @ '+window.innerWidth;
        debug_div.innerHTML += '(+'+window.pageXOffset+')';
        debug_div.innerHTML += ','+window.innerHeight;
        debug_div.innerHTML += '(+'+window.pageYOffset+')';
      }
    }

    if (debug) {
      window.addEventListener('scroll', update_debug_div);
    }

    function scale_and_position(is_jump) {
      /* To keep the image centered at all times, we compensate its
       * movement during scaling with an appropriate translation to keep
       * it top-left corner in place, and we the container's left and top
       * css properties to center it while the image is still smaller
       * than the window. After a dimension grows bigger than the
       * window's, we center the window instead, via scrolling.
       *
       * Notice that we need the image container in order to have the
       * document's body only see an element the size of the scaled
       * image. Without the container and with a large downscaled image,
       * the body will grow enough to contain the original image's
       * dimensions, leaving blank space to its right and/or bottom.
       * Since the container's dimensions follow the scaled image's and
       * its overflow is hidden, we get the exact same behavior from it
       * as we would get from a small image the size of the downscaled
       * image, which is precisely what we want. */

      /* keep image's top-left corner in place (at container's top-left)
       * during scaling */
      translate_x = Math.round(image.width*(1-1/new_scale)/2);
      translate_y = Math.round(image.height*(1-1/new_scale)/2);

      image.style[transform] = 'scale('+new_scale+','+new_scale+') translate('+translate_x+'px,'+translate_y+'px)';

      // fit container to scaled image
      image_container.style.width = image.width*new_scale;
      image_container.style.height = image.height*new_scale;

      /* center container, but only until it hits the window's boundaries;
       * after that, center window instead */
      // FIXME: when scrolling, keep window center in place, not image
      // center
      scroll_left = initial_scroll_left;
      scroll_top = initial_scroll_top;
      if (image.width*new_scale <= window.innerWidth) {
        scroll_left = 0;

        // center container by offsetting its dimension by half of how
        // bigger the window's is
        image_container.style.left = -Math.round((image.width*new_scale - window.innerWidth)/2);
      }
      else {
        image_container.style.left = 0;

        /* when scaling to 100% or coming from a scaled dimension
         * smaller than the window, center window instead of maintaing
         * the current scroll, otherwise, scroll window from current
         * position */
        if (is_jump || image.width*start_scale <= window.innerWidth) {
          scroll_left = Math.round((image.width*new_scale - window.innerWidth)/2);
        }
        else {
          // scroll window by half of how much the image grew
          var width_delta = image.width*new_scale - image.width*start_scale;
          scroll_left = initial_scroll_left + Math.round(width_delta/2);
        }
      }

      if (image.height*new_scale <= window.innerHeight) {
        scroll_top = 0;

        // center container by offsetting its dimension by half of how
        // bigger the window's is
        image_container.style.top = -Math.round((image.height*new_scale - window.innerHeight)/2);
      }
      else {
        image_container.style.top = 0;

        /* when scaling to 100% or coming from a scaled dimension
         * smaller than the window, center window instead of maintaing
         * the current scroll, otherwise, scroll window from current
         * position */
        if (is_jump || image.height*start_scale <= window.innerHeight) {
          scroll_top = Math.round((image.height*new_scale - window.innerHeight)/2);
        }
        else {
          // scroll window by half of how much the image grew (regular
          // centering)
          var height_delta = image.height*new_scale - image.height*start_scale;
          scroll_top = initial_scroll_top + Math.round(height_delta/2);
        }
      }

      if (scroll_left < 0) { scroll_left = 0; }
      if (scroll_top < 0) { scroll_top = 0; }
      window.scrollTo(scroll_left, scroll_top);

      if (debug) { update_debug_div(); }
    }

    function distance_to_origin(point) {
      /* square root of the sum of the squares */
      var origin = [0, 0];
      var result = 0;
      // ignore one axis if the user chose to do so
      if (resize_on_x_axis) {
        result += Math.pow(point[0]-origin[0], 2);
      }
      if (resize_on_y_axis) {
        result += Math.pow(point[1]-origin[1], 2);
      }
      return Math.sqrt(result);
    }

    function scale_image(mouse_delta) {
      /* scale image according to how much the mouse moved */

      /* scale multiplies the image's dimensions, so we calculate the
       * new scale based on how many pixels we want to add (or
       * subtract) to the image's diagonal */
      var image_diagonal = Math.sqrt(Math.pow(image.scrollWidth, 2) + Math.pow(image.scrollHeight, 2));

      // update scale rate according to current scale
      set_new_scale_rate();

      var new_scale_candidate = start_scale + (mouse_delta*scale_rate)/image_diagonal;

      // enforce minimum scale (chosen arbitrarily)
      var minimum_dimension = 20;
      if (image.width*new_scale_candidate >= minimum_dimension &&
          image.height*new_scale_candidate >= minimum_dimension) {
        new_scale = new_scale_candidate;
      }

      fitted_to_window = false;
      scale_and_position(false);
    }

    //mousedrag-events
    function scale_on_mouse_drag(event) {
      if (!mouse_resize_enabled) {
        return;
      }

      // only left button
      if (event.button !== 0) {
        return;
      }

      //starting coordinates
      var start_point = [event.clientX, event.clientY];

      // starting window position
      initial_scroll_left = scroll_left = window.pageXOffset;
      initial_scroll_top = scroll_top = window.pageYOffset;

      // define these here so all handlers can access them
      var might_be_click = true;
      function set_resize_cursor() {
        if (might_be_click) {
          might_be_click = false;
          var cursor = 'default';
          if (resize_on_x_axis && resize_on_y_axis) {
            cursor = 'se-resize';
          }
          else if (resize_on_x_axis) {
            cursor = 'e-resize';
          }
          else if (resize_on_y_axis) {
            cursor = 's-resize';
          }
          image.style.cursor = cursor;
        }
      }
      var timer = window.setTimeout(set_resize_cursor, 200);

      function mouse_move_handler(event) {
        var current_point = [event.clientX, event.clientY];

        var mouse_delta = distance_to_origin(current_point) - distance_to_origin(start_point);

        // chosen arbitrarily as the amount the user might
        // accidentally move when actually trying to only click
        var mouse_delta_tolerance = 3;
        if (Math.abs(current_point[0]-start_point[0]) > mouse_delta_tolerance ||
            Math.abs(current_point[1]-start_point[1]) > mouse_delta_tolerance) {
          set_resize_cursor();
        }

        if (!might_be_click) {
          //rescale
          scale_image(mouse_delta);

          event.preventDefault();
          return false;
        }
      }

      function mouse_up_handler(event) {
        start_scale = new_scale;
        image.style.cursor = 'default';
        document.removeEventListener('mousemove', mouse_move_handler);
        document.removeEventListener('mouseup', mouse_up_handler);
        window.clearTimeout(timer);

        if (might_be_click) {
          if (debug) { console.log('caught a click'); }
          scale_to_fit();
        }

        event.preventDefault();
        return false;
      }

      document.addEventListener('mousemove', mouse_move_handler);
      document.addEventListener('mouseup', mouse_up_handler);

      event.preventDefault();
      return false;
    }

    image.addEventListener('mousedown', scale_on_mouse_drag);

    /* stop browser from hijacking our clicks */
    function browser_hijacking_mitigation() {
      if (image.width !== natural_height || image.height !== natural_height) {
        if (debug) { console.log('blocking browser from scaling image to '+image.width+'x'+image.height); }
        image.width = natural_width;
        image.height = natural_height;
      }

      // this can be zoom-in, zoom-out, -webkit-zoom-in and -webkit-zoom-out
      if (/zoom/.test(image.style.cursor)) {
        image.style.cursor = 'default';
      }

      if (window.pageXOffset !== scroll_left || window.pageYOffset !== scroll_top) {
        if (debug) { console.log('blocking browser from scrolling to '+window.pageXOffset+','+window.pageYOffset); }
        window.scrollTo(scroll_left, scroll_top);
      }
    }
    image.addEventListener('click', function(event) {
      browser_hijacking_mitigation();
      event.preventDefault();
      return false;
    });

    function get_fit_scale() {
      // scale to fit horizontally (more common so this is done first)
      var scale = window.innerWidth/(image.width);
      while (image.width*scale > window.innerWidth) {
        scale -= 0.001;
      }
      // scale to fit vertically (image_height*start_scale to accommodate to
      // the potential horizontal scaling above)
      if (image.height*scale > window.innerHeight) {
        scale = window.innerHeight/(image.height);
        while (image.height*scale > window.innerHeight) {
          scale -= 0.001;
        }
      }
      return scale;
    }

    // toggle between fit and 100% on double click
    function scale_to_fit() {
      /* default to fit for images bigger than window, and to 100% for
       * images smaller than window */

      if (image.width > window.innerWidth || image.height > window.innerHeight) {
        // fit to window if not already fitted
        if (!fitted_to_window) {
          start_scale = new_scale = get_fit_scale();
          fitted_to_window = true;
         }
        // otherwise reset scale to 100%
        else {
          new_scale = start_scale = 1;
          fitted_to_window = false;
        }
      }
      else { // image smaller than window
        // reset scale to 100% if not already at it
        if (new_scale !== 1) {
          new_scale = start_scale = 1;
          fitted_to_window = false;
        }
        // otherwise, scale to fit window
        else {
          start_scale = new_scale = get_fit_scale();
          fitted_to_window = true;
        }
      }
      scale_and_position(true);
    }

    /* keyboard */
    document.addEventListener('keypress', function(event) {
      if (!hotkeys_enabled) { return; }

      var char_pressed;
      /* regular keys will have keyCode == 0 and charCode == the key's code,
       * while special keys (F<n>, enter, tab, ...) will be the other way
       * around */
      if (event.charCode > 0) {
        char_pressed = String.fromCharCode(event.charCode);
        if (debug) { console.log('Got key pressed: "'+char_pressed+'" (charcode='+event.charCode+')'); }
      }
      else {
        char_pressed = event.keyCode;
        if (debug) { console.log('Got key pressed: '+char_pressed); }
      }

      if (char_pressed in hotkeys) {
        if (hotkeys[char_pressed] == 'zoom_in') {
          scale_image(hotkey_zoom_delta);
          start_scale = new_scale;
          fitted_to_window = false;
        }
        else if (hotkeys[char_pressed] == 'zoom_out') {
          scale_image(-hotkey_zoom_delta);
          start_scale = new_scale;
          fitted_to_window = false;
        }
        else if (hotkeys[char_pressed] == 'toggle_fit') {
          scale_to_fit();
        }
        else if (hotkeys[char_pressed] == 'fit_to_window') {
          start_scale = new_scale = get_fit_scale();
          scale_and_position(true);
          fitted_to_window = true;
        }
        else if (hotkeys[char_pressed] == 'reset_zoom') {
          start_scale = new_scale = 1;
          scale_and_position(true);
          fitted_to_window = false;
        }
        else if (hotkeys[char_pressed] == 'disable_mouse_resize') {
          /* toggle mouse resize */
          mouse_resize_enabled = !mouse_resize_enabled;
        }

        event.preventDefault();
        return false;
      }
    });


    /* initial values */
    var starting_width = image.width;
    var starting_height = image.height;
    function apply_starting_values() {
      scale_to_fit();

      // reposition on window resize
      window.addEventListener('resize', function(event) {
        browser_hijacking_mitigation();

        // if fitted to window, refit to new window dimensions
        if (fitted_to_window) {
          start_scale = new_scale = get_fit_scale();
          scale_and_position(true);
        }
        else {
          /* we need to set the initial scroll for scale_and_position, because
           * there was no mousedown event to have done it already */
          initial_scroll_left = scroll_left;
          initial_scroll_top = scroll_top;
          scale_and_position(false);
        }

        event.preventDefault();
        return false;
      });

      document.addEventListener('scroll', function() {
        /* keep track of current position in case we need to stop the browser
         * from scolling on click */
        if (scroll_left !== window.pageXOffset || scroll_top !== window.pageYOffset) {
          scroll_left = window.pageXOffset;
          scroll_top = window.pageYOffset;
        }
      });

      // wait for image to load and check that its dimensions are correct
      // this will neve fire if the image has already loaded by now
      image.addEventListener('load', function() {
        if (debug) { console.log('image finished loading and it is '+natural_width+'x'+natural_height+' ('+image.width+'x'+image.height+')'); }

        if (image.width != starting_width || image.height != starting_height) {
          if (debug) { console.log('image changed dimensions, redrawing'); }
          scale_and_position(true);
        }
      });
    }

    apply_starting_values();
  }

  function wait_for_image(image) {
    /* wait for image to load enough for us to know its dimensions
     * (unfortunately, there's no event available for this, only onload,
     * which fires after the whole image has loaded, and that might take a
     * long time) */
    if (image.width === 0 || image.height === 0 ||
        image.naturalWidth === 0 || image.naturalHeight === 0) {
      if (debug) { console.log('waiting for image to load enough'); }
      window.setTimeout(function() {
        wait_for_image(image);
      }, 100);
      return;
    }
    if (debug) { console.log('image is '+image.naturalWidth+'x'+image.naturalHeight+' (browser wants to rescale to '+image.width+'x'+image.height+')'); }

    /* check if image is rotated and store width and height (chrome switches
     * naturalWidth and naturalHeight when auto rotating the image, but firefox
     * doesn't, so we have to check) */
    natural_width = image.naturalWidth;
    natural_height = image.naturalHeight;
    /* if difference between current and rotated aspects is smaller than
     * non-rotated, consider the image rotated, e.g.
     * (where |x| means absolute value of x)
     * |(1280/720)-(1920/1080)| < |(1280/720)-(1080/1920)|, but
     * |(720/1280)-(1920/1080)| > |(720/1280)-(1080/1920)| */
    var aspect = image.width/image.height,
      natural_aspect = natural_width/natural_height,
      rotated_aspect = natural_height/natural_width;
    if (Math.abs(aspect-rotated_aspect) <
        Math.abs(aspect-natural_aspect)) {
      if (debug) { console.log('image appears to be rotated'); }
      natural_width = image.naturalHeight;
      natural_height = image.naturalWidth;
    }

    // browser rescaling mitigation
    image.width = natural_width;
    image.height = natural_height;
    image.style.cursor = 'default';
    setup_controls();
  }

  function check_body() {
    /* waits for document.body to load before checking it */
    if (document.body === null) {
      if (debug) { console.log('waiting for body'); }
      window.setTimeout(check_body, 100);
      return;
    }

    // domcontentloaded shouldn't fire more than once, but just to make sure
    document.removeEventListener('DOMContentLoaded', check_body);

    if (debug) {
      console.log('elements in page: '+document.body.childNodes.length);
      console.log('images in page: '+document.getElementsByTagName("img").length);
    }

    if (document.body.childNodes.length !== 1) { return; }
    if (document.body.firstChild.tagName !== 'IMG') { return; }

    wait_for_image(document.body.firstChild);
  }

  /* check that the document consists of a single image */
  if (document.body !== null) {
    check_body();
  }
  else {
    if (debug) { console.log('waiting for document to load'); }
    document.addEventListener('DOMContentLoaded', check_body);
  }
}());
