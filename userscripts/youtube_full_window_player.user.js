// ==UserScript==
// @name        Youtube - full window player
// @namespace   https://gitlab.com/loopvid/scripts
// @description Allow expanding the player so that it occupies the whole window.
// @include     http*://*.youtube.com/*
// @include     http*://*.dailymotion.com/*
// @version     1.2.1
// @grant       none
// @run-at      document-start
// ==/UserScript==

/* USAGE:
 *
 * Visit https://www.youtube.com/html5 and request the HTML5 player if needed.
 * This script will not work on the regular flash player. Flash users are
 * advised to use the video's embedded page instead (i.e. make
 * https://www.youtube.com/watch?v=<video_id>
 * into
 * https://www.youtube.com/v/<video_id> ).
 *
 * We will position a small toggle on the top left corner of the screen on
 * youtube/dailymotion watch pages, which when clicked will expand the player
 * to occupy the whole screen or shrink it back to its original size and
 * position. If hotkeys are enabled, you can use 'w' to toggle full window mode
 * and esc to exit it only.
 *
 * When the player is expanded, another small button will be added to the top
 * left corner for rotating the video 90 degrees clockwise. The 'r' hotkey will
 * also become available for the same purpose. Note that rotation only affects
 * the video when expanded, and that it is not preserved when navigating to a
 * different video.
 */

(function() {
  'use strict';

  var enable_hotkeys = true,
      last_href = null,
      is_expanded = false,
      rotation = 0;

  // don't run on frames
  // http://stackoverflow.com/a/4190594
  if (window.top != window.self) { return; }

  function expand_player() {
    /* expand the player to fill the whole window */
    var button = document.getElementById('full_window_button'),
        player = document.querySelector(get_player_selector());

    player.classList.add('full_window_player');
    document.body.classList.add('player_expanded');

    button.className = 'full_window_exit';
    button.innerHTML = '↮';

    is_expanded = true;
  }

  function shrink_player() {
    /* shrink full window player back to its normal size */
    var button = document.getElementById('full_window_button'),
        player = document.querySelector(get_player_selector());

    document.body.classList.remove('player_expanded');

    if (player) {
      player.classList.remove('full_window_player');
    }

    if (button) {
      button.className = 'full_window_enter';
      button.innerHTML = '↔';
    }

    is_expanded = false;
  }

  function rotate_video() {

    var player = document.querySelector(get_player_selector());

    if (!(document.body.classList.contains('player_expanded') && player)) {
      return;
    }

    // undo previous rotation
    if (rotation) {
      player.classList.remove('rot'+rotation);
    }

    // rotate 90 degrees clockwise
    rotation += 90;
    rotation %= 360;

    // apply new rotation (do nothing if we're back at 0 degrees)
    if (rotation) {
      player.classList.add('rot'+rotation);
    }
  }

  function undo_rotation() {
    var player = document.querySelector(get_player_selector());

    if (rotation && player && player.classList.contains('rot'+rotation)) {
      player.classList.remove('rot'+rotation);
    }

    rotation = 0;
  }

  function expand_button_click_handler(event) {
    // only capture clicks for the left button
    if (event.button !== 0) { return; }
    // only capture unmodified clicks
    if (event.ctrlKey || event.altKey || event.shiftKey || event.metaKey) { return; }

    if (is_expanded) {
      shrink_player();
    }
    else {
      expand_player();
    }
  }

  function rotate_button_click_handler(event) {
    // only capture clicks for the left button
    if (event.button !== 0) { return; }
    // only capture unmodified clicks
    if (event.ctrlKey || event.altKey || event.shiftKey || event.metaKey) { return; }

    rotate_video();
  }

  function keypress_handler(event) {
    /* check if keypress is one of our shortcuts and, if so, handle it */

    var player = document.querySelector(get_player_selector());

    // never capture a key from an input
    if (event.target.tagName === 'INPUT') { return; }
    /* only capture keys targetted at an element that contains the player or is
     * contained by it (or is the player itself or our own button) */
    if (!(event.target.contains(player) || player.contains(event.target) ||
          event.target === player || event.target.id === 'full_window_button')) {
      return;
    }

    // esc
    if (event.key == 'Escape' || event.keyCode == 27) {
      // exit full window mode
      if (is_expanded) {
        shrink_player();
      }
    }
    // use 'w' for full [W]indow mode (since youtube already uses 'f')
    else if (event.key == 'w' || event.keyCode == 87) {
      // toggle full window mode
      if (is_expanded) {
        shrink_player();
      }
      else {
        expand_player();
      }
    }
    else if (event.key == 'r' || event.keyCode == 82) {
      if (is_expanded) {
        rotate_video();
      }
    }
    // not one of our hotkeys, just ignore it
    else { return; }

    // stop propagation (only if we did handle the keypress)
    event.preventDefault();
    event.stopPropagation();
  }

  function create_buttons() {
    if (document.getElementById('full_window_button')) { return; }

    var outer_div, button;

    outer_div = document.createElement('DIV');
    outer_div.id = 'full_window_buttons';

    button = document.createElement('DIV');
    button.className = 'full_window_enter';
    button.innerHTML = '↔';
    button.id = 'full_window_button';
    button.addEventListener('click', expand_button_click_handler);
    outer_div.appendChild(button);

    button = document.createElement('DIV');
    button.className = 'full_window_rotate';
    button.innerHTML = '↷';
    button.id = 'full_window_rotate_button';
    button.addEventListener('click', rotate_button_click_handler);
    outer_div.appendChild(button);

    document.body.appendChild(outer_div);
  }

  function destroy_buttons() {
    var button = document.getElementById('full_window_button');
    if (button) {
      button.parentNode.removeChild(button);
    }
  }

  function is_watch_page() {
    /* check if this is a watch page */
    if (is_youtube()) {
      return (window.location.pathname.search('watch') >= 0);
    }
    else {
      // dailymotion
      return (window.location.pathname.search('video') >= 0);
    }
  }

  function is_html5_player() {
    /* check if the html5 player is being used */
    var player = document.querySelector(get_player_selector());
    return (player.tagName !== 'EMBED');
  }

  function maintain_body_class() {
    /* if in full window mode, check that the body still has the
     * player_expanded class (since youtube's js wipes it on page changes) */
    if (is_expanded && !document.body.classList.contains('player_expanded')) {
      document.body.classList.add('player_expanded');
    }
  }

  function set_up_button() {
    if (window.location.href === last_href) { return; }

    if (!is_watch_page()) {
      // don't check again for this page
      last_href = window.location.href;

      destroy_buttons();

      /* if we're currently in full window mode, exit it */
      if (is_expanded) {
        shrink_player();
      }
    }
    else if (is_youtube() && !is_html5_player()) {
      /* watch page, but no html5 player: destroy button but keep checking in
       * case the flash player gets replaced later */
      destroy_buttons();

      /* if we're currently in full window mode, exit it */
      if (is_expanded) {
        shrink_player();
      }
    }
    else {
      // don't check again for this page
      last_href = window.location.href;

      // if button doesn't already exist, create it
      if (!document.getElementById('full_window_button')) {
        create_buttons();
      }

      /* if we're currently in full window mode, re-run expand_player() to make
       * sure our classes will continue to be in place */
      if (is_expanded) {
        expand_player();
      }

      // undo rotation, if applicable, since we don't want to preserve it
      // across different videos
      undo_rotation();
    }
  }

  function set_up_css() {
    /* insert new sheet for our custom rules */
    var elem, rules, i, transforms;

    elem = document.createElement('style');
    document.head.appendChild(elem);

    transforms = {
      '90': 'translate(calc(50vw - 50vh), calc(50vh - 50vw)) rotate(90deg)',
      '180': 'rotate(180deg)',
      '270': 'translate(calc(50vw - 50vh), calc(50vh - 50vw)) rotate(270deg)'
    };

    if (is_youtube()) {
      rules = [
        'body.player_expanded { overflow: hidden; }',
        'body.player_expanded app-header, '+
          'body.player_expanded #masthead-positioner, '+
          'body.player_expanded #masthead-container, '+
          'body.player_expanded iframe { display: none !important; }',
        '.full_window_player { position: fixed !important; '+
          'z-index: 2 !important; background-color: #000; }',
        '.full_window_player .html5-video-container { height: 100%; }',
        '.full_window_player .ytp-chrome-bottom { left: 50% !important; '+
          'transform: translate(-50%, 0px); }',
        // for vertical videos and full screen, youtube will try to use top and
        // left to center the video, which we must override in full window mode
        '.full_window_player video { top: 0px !important; left: 0px !important; }',
        'body.player_expanded #opt-out-dialog, '+
          'body.player_expanded yt-live-chat-header-renderer, '+
          'body.player_expanded .watch-sidebar button { display: none; }'
      ];
    }
    else {
      // for dailymotion
      rules = [
        'body.player_expanded { overflow: hidden; }',
        'body.player_expanded div[class*="Header__container"] { display: none; }',
        'body.player_expanded .dmp_VideoView-content { '+
          'top: 0px !important; left: 0px!important; width: 100vw !important; '+
          'height: 100vh !important; }'
      ];
    }

    rules = rules.concat([
        '#full_window_buttons { vertical-align: middle; position: fixed; '+
          'left: 4px; top: 0px; opacity: .3; font-size: large; '+
          'cursor: pointer; width: 1em; color: #888; }',
        '#full_window_buttons:hover { opacity: 1; }',
        // thanks to #masthead-positioner, otherwise 941 would have been enough
        '#full_window_buttons {  z-index: 2000000000; }',
        '#full_window_button { display: inline-block; }',
        '#full_window_rotate_button { display: none; }',
        '.player_expanded #full_window_rotate_button { display: inline-block; }',

        // dimensions and tranforms for rotations
        '.full_window_player { top: 0px !important; left: 0px !important; '+
          'width: 100vw !important; height: 100vh !important; }',
        '.full_window_player.rot90 video { transform: '+transforms['90']+'; }',
        '.full_window_player.rot180 video { transform: '+transforms['180']+'; }',
        '.full_window_player.rot270 video { transform: '+transforms['270']+'; }',
        '.full_window_player:not(.rot90):not(.rot270) video { '+
          'width: 100vw !important; height: 100vh !important; }',
        // invert width and height
        '.full_window_player.rot90 video, .full_window_player.rot270 video { '+
          'width: 100vh !important; height: 100vw !important; }'
        ]);

    for (i=0; i < rules.length; i++) {
      elem.sheet.insertRule(rules[i], i);
    }
  }

  function set_up_hotkeys() {
    /* set up keyboard shortcuts for the full window player */

    if (!enable_hotkeys) { return; }

    document.addEventListener('keypress', keypress_handler);
  }

  function is_youtube() {
    return window.location.hostname.search('youtube') >= 0;
  }

  function is_youtube_gaming() {
    return window.location.hostname.search('gaming.youtube') >= 0;
  }

  function get_player_selector() {
    // return the element id for the player
    if (is_youtube()) {
      if (is_youtube_gaming()) {
        return '#player';
      }
      else {
        return '#movie_player';
      }
    }
    else {
      // dailymotion
      return '#root>div>div.Player';
    }
  }

  function set_up_mutation_observers() {
    var observer;

    /* observe document for changes in the page so we know when we're going
     * into and out of watch pages */
    observer = new MutationObserver(set_up_button);
    observer.observe(document, {childList: true, subtree: true});

    /* observe the body for attribute changes, so we know if youtube's js has
     * wiped out our own class */
    observer = new MutationObserver(maintain_body_class);
    observer.observe(document.body, {attributes: true});
  }

  function initial_setup() {
    set_up_mutation_observers();
    set_up_css();
    set_up_hotkeys();
    set_up_button();
  }

  document.addEventListener('DOMContentLoaded', initial_setup);
}());
