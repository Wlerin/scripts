// ==UserScript==
// @name          List links
// @namespace     https://gitlab.com/loopvid/scripts
// @description   List links in a page
// @include       http*://*
// @version       1.0
// @grant         none
// ==/UserScript==

/*
 * Usage:
 *
 * Press ctrl-shift-L to open the search panel on the bottom-right corner of
 * the window.
 *
 * The first input should contain a comma-separated list of the html tag names
 * whose elements you want to search.
 *
 * The second input should contain a comma-separated list of all the attributes
 * whose values should be tested.
 *
 * The third input should contain the search term, which can be a simple
 * substring or a regular expression, as controlled by the select below it.
 *
 */

(function() {
  'use strict';

  // don't run on frames
  if (window.top != window.self) { return true; }

  var menu_elem, output_elem,
      div_style = (
        'display: inline-block; '+
        'background-color: #fff; '+
        'right: 15px; '+
        'bottom: 0px; '+
        'position: fixed; '+
        'padding: 5px; '+
        'border: 1px solid #000; '+
        'z-index: 1000;'
      );

  function remove_menu() {
    if (menu_elem) {
      menu_elem.parentNode.removeChild(menu_elem);
      menu_elem = null;
    }
  }

  function remove_output() {
    if (output_elem) {
      output_elem.parentNode.removeChild(output_elem);
      output_elem = null;
    }
  }

  function search(params) {
    var i, j, k, pattern, form, elems, value, found,
        seen = {},
        urls = [];

    console.log('creating regexp');

    pattern = params.pattern;
    if (params.pattern_type === 'regex') {
      try {
        pattern = new RegExp(params.pattern);
      } catch(e) {
        form = menu_elem.querySelector('form');
        form.appendChild(document.createTextNode('Bad pattern'));
        return;
      }
    }

    console.log('searching');

    for (i=0; i<params.elems.length; i++) {
      elems = document.getElementsByTagName(params.elems[i]);
      for (j=0; j<elems.length; j++) {
        for (k=0; k<params.attrs.length; k++) {
          value = elems[j][params.attrs[k]];

          if (value) {
            // search according to the pattern type
            if (params.pattern_type === 'regex') {
              found = value.search(pattern) > -1;
            }
            else {
              found = value.indexOf(pattern) > -1;
            }

            if (found && !seen[value]) {
              urls.push(value);
              seen[value] = true;
            }
          }
        }
      }
    }

    console.log('done');

    remove_menu();
    build_output_elem(params, urls);
  }

  function handle_submit(event) {
    /* extract search parameters and perform search */

    console.log('handle submit');

    var form,
        inputs = {},
        params = {};

    if (event) {
      event.preventDefault();
      event.stopPropagation();
    }

    if (menu_elem) {
      form = menu_elem.querySelector('form');
      if (form) {
        inputs.elems = form.querySelector('input[name="elems"]');
        inputs.attrs = form.querySelector('input[name="attrs"]');
        inputs.pattern = form.querySelector('input[name="pattern"]');
        inputs.pattern_type = form.querySelector('select[name="pattern_type"]');
      }
    }

    if (!(inputs.elems && inputs.attrs &&
          inputs.pattern && inputs.pattern_type)) {
      console.log('error finding inputs');
      remove_menu();
      return;
    }

    params.elems = inputs.elems.value.split(',');
    params.attrs = inputs.attrs.value.split(',');
    params.pattern = inputs.pattern.value;
    params.pattern_type = inputs.pattern_type.value;

    if (params.elems.length === 0 || params.attrs.length === 0 ||
        !params.pattern) {
      form.appendChild(document.createTextNode('Please input all values'));
      return;
    }

    search(params);
  }

  function build_output_elem(params, urls) {
    console.log('build output elem');

    var button, textarea;

    if (output_elem) {
      remove_output();
    }

    output_elem = document.createElement('div');

    output_elem.innerHTML = (
      '<div><textarea cols="50" rows="10"></textarea></div>'+
      '<button class="new_search">Search again</button>'+
      '<button class="close">Close</button>'
    );
    output_elem.style = div_style;

    console.log('filling out textarea');

    textarea = output_elem.querySelector('textarea');

    if (urls.length > 0) {
      textarea.value = urls.join('\n');
    }
    else {
      textarea.value = 'No results found';
    }

    console.log('setting up buttons');

    button = output_elem.querySelector('button.close');
    button.addEventListener('click', remove_output);

    button = output_elem.querySelector('button.new_search');
    button.addEventListener('click', function() {
      remove_output();
      build_menu(params);
    });

    document.body.appendChild(output_elem);
  }

  function build_menu(params) {
    console.log('build menu');

    var form;

    if (menu_elem) {
      remove_menu();
    }

    menu_elem = document.createElement('div');

    menu_elem.innerHTML = (
      '<form>'+
      '<div>Search links</div>'+
      '<div>Elements: <input type="text" name="elems" value="a,img" /></div>'+
      '<div>Attributes: <input type="text" name="attrs" value="href,src" /></div>'+
      '<div>Pattern: <input type="text" name="pattern" /></div>'+
      '<input type="submit" style="display: none;" />'+
      '<div><select name="pattern_type">'+
        '<option value="substr" selected>Substring</option>'+
        '<option value="regex">Regular expression</option>'+
      '</select></div>'+
      '</form>'+
      '<div><button class="submit">Search</button>'+
        '<button class="cancel">Cancel</button></div>'
    );
    menu_elem.style = div_style;

    console.log('setting up event listeners');

    form = menu_elem.querySelector('form');
    form.addEventListener('submit', handle_submit);

    menu_elem.querySelector('button.submit').addEventListener(
      'click', handle_submit);

    menu_elem.querySelector('button.cancel').addEventListener(
      'click', remove_menu);

    // reinsert previous values, if available
    if (params) {
      if (params.elems) {
        form.querySelector('input[name="elems"]').value = params.elems.join(',');
      }

      if (params.attrs) {
        form.querySelector('input[name="attrs"]').value = params.attrs.join(',');
      }

      if (params.pattern) {
        form.querySelector('input[name="pattern"]').value = params.pattern;
      }

      if (params.pattern_type) {
        form.querySelector(
          'select[name="pattern_type"]').value = params.pattern_type;
      }
    }

    console.log('appending element');

    document.body.appendChild(menu_elem);
  }

  function keypress_handler(event) {
    //console.log(
    //  'got keypress: which='+event.which+' char='+event.char+' key='+event.key+
    //  ' shiftkey='+event.shiftKey+' ctrl='+event.ctrlKey);

    // ctrl-shift-L
    if (!((event.key === 'L' || event.which === 76) &&
          event.shiftKey && event.ctrlKey)) {
      return;
    }

    console.log('got shortcut');

    event.preventDefault();
    event.stopPropagation();

    if (!menu_elem) {
      // remove output element if present
      if (output_elem) {
        remove_output();
      }

      build_menu();
    }
    else {
      remove_menu();
    }
  }

  document.body.addEventListener('keypress', keypress_handler);

  console.log('ready to search');

}());
