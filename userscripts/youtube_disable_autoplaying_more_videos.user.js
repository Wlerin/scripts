// ==UserScript==
// @name          Youtube - disable autoplaying more videos
// @namespace     https://gitlab.com/loopvid/scripts
// @description   Automatically disable autoplaying another video once the one you're watching ends
// @include       https://www.youtube.com/*
// @include       http://www.youtube.com/*
// @version       1.3
// @grant         none
// @run-at        document-start
// ==/UserScript==

(function() {
  'use strict';

  // don't run on frames
  // http://stackoverflow.com/a/4190594
  if (window.top != window.self) { return; }

  function do_click(elem) {
    /* dispatch a click on an element */
    var evt = new MouseEvent(
      'click', { bubbles: true, cancelable: true, view: window});

    elem.dispatchEvent(evt);
  }

  function setup() {
    /* if the autoplay checkbox is checked, uncheck it */
    if (window.location.pathname.search('watch') < 0) { return; }

    var i, toggle, ids = ['autoplay-checkbox', 'improved-toggle'];

    for (i=0; i<ids.length; i++) {

      toggle = document.getElementById(ids[i]);

      if (!toggle) { continue; }

      if (toggle.checked === true) {
        // click it
        console.log('toggling autoplay');
        do_click(toggle);
      }
    }
  }

  function initial_setup() {
    /* keep watching the page for mutations and checking the autoplay checkbox
     * every time */
    var observer = new MutationObserver(setup);
    observer.observe(document, { childList: true, subtree: true });

    setup();
  }

  document.addEventListener('DOMContentLoaded', initial_setup);
}());
