// ==UserScript==
// @name        Twitter photographer ripper
// @namespace   https://gitlab.com/loopvid/scripts
// @description Twitter photographer ripper
// @include     https://twitter.com/*
// @version     1.1
// @grant       none
// ==/UserScript==

(function() {
  'use strict';

  // don't run on frames
  if (window.top != window.self) { return true; }

  var menu_elem, output_elem,
    div_style = (
      'display: inline-block; '+
      'background-color: #fff; '+
      'right: 15px; '+
      'bottom: 0px; '+
      'position: fixed; '+
      'padding: 5px; '+
      'border: 1px solid #000; '+
      'z-index: 1000;'
    ),
    button_classes = 'EdgeButton EdgeButton--primary',
    bad_char_regex = /[\\/:*?"<>|;]/g,
    url_regex = / ?(https?:\/\/|pic\.twitter\.com)[^ #]+/g,
    path_len_limit = 250;

  function remove_menu() {
    if (menu_elem) {
      menu_elem.parentNode.removeChild(menu_elem);
      menu_elem = null;
    }
  }

  function remove_output() {
    if (output_elem) {
      output_elem.parentNode.removeChild(output_elem);
      output_elem = null;
    }
  }

  function search(params) {
    var form, i, j, posts, text, elems, image_urls, tistory_links, match,
      username,
      pattern = null,
      date_regex = /\b\d{6}\b/,
      found = [];

    console.log('creating regexp');

    if (params.pattern) {
      pattern = params.pattern;
      if (params.pattern_type === 'regex') {
        try {
          pattern = new RegExp(params.pattern);
        } catch(e) {
          form = menu_elem.querySelector('form');
          form.appendChild(document.createTextNode('Bad pattern'));
          return;
        }
      }
    }

    console.log('searching');

    posts = document.querySelectorAll('div.tweet');
    for (i=0; i < posts.length; i++) {
      // skip retweets
      if (posts[i].querySelector('.js-retweet-text')) { continue; }
      // skip promoted tweets
      if (posts[i].querySelector('.promoted-tweet-heading')) { continue; }

      // find images
      elems = posts[i].querySelectorAll('div[data-image-url]');

      image_urls = [];
      for (j=0; j < elems.length; j++) {
        image_urls.push(elems[j].dataset.imageUrl+':orig');
      }

      // find tistory links
      elems = posts[i].querySelectorAll(
        'a.twitter-timeline-link[data-expanded-url*="tistory"]');

      tistory_links = [];
      for (j=0; j < elems.length; j++) {
        tistory_links.push(elems[j].dataset.expandedUrl);
      }

      // skip text only tweets without any interesting links
      if (image_urls.length === 0 && tistory_links.length === 0) { continue; }

      text = posts[i].querySelector('p.tweet-text').textContent;

      // skip non-matching tweets, when applicable
      if (pattern) {
        if (!(text && (
            params.pattern_type == 'regex' && text.search(pattern) > -1 ||
            params.pattern_type == 'substr' && text.indexOf(pattern) > -1))) {
          continue;
        }
      }

      username = posts[i].querySelector('.username').textContent.replace('@', '');
      if (!username) {
        console.log("couldn't extract username");
        continue;
      }

      // try to find date in the text
      match  = date_regex.exec(text);

      found.push({
        text: text,
        date: match ? match[0] : null,
        images: image_urls,
        tistory: tistory_links,
        username: username
      });
    }

    console.log('done');

    remove_menu();

    build_output_elem(params, format_output(found));
  }

  function format_output(tweets) {
    /* build a semi-colon separated csv containing url, username and output
     * directory. */
    var i, j, dirname, tweet, date, username,
      output = '',
      whitespace_regex = /( {2,}|\n)/g;

    for (i=0; i < tweets.length; i++) {
      tweet = tweets[i];

      username = tweet.username.replace(bad_char_regex, '_');

      // build dirname based on the tweet's text
      dirname = tweet.text.replace(url_regex, '').replace(bad_char_regex, '_');

      // move date to the beginning when applicable
      date = tweet.date;
      if (date && dirname.indexOf(date) > 0) {
        dirname = date+' '+dirname.replace(date, '');
      }

      // collapse multiple spaces and replace linebreaks
      dirname = dirname.replace(whitespace_regex, ' ').trim();

      // try to keep path at a safe length for windows (total path length limit
      // is 260 characters, twitter text length limit is 280)
      dirname = dirname.slice(0, path_len_limit-(username.length+1));

      // build one line per image found
      for (j=0; j < tweet.images.length; j++) {
        // url;username;filename
        output += tweet.images[j]+';'+username+';'+dirname+'\n';
      }

      // also one line per tistory link found
      for (j=0; j < tweet.tistory.length; j++) {
        // url;username;filename
        output += tweet.tistory[j]+';'+username+';'+dirname+'\n';
      }
    }

    return output;
  }

  function handle_submit(event) {
    /* extract search parameters and perform search */

    console.log('handle submit');

    var form,
        inputs = {},
        params = {};

    if (event) {
      event.preventDefault();
      event.stopPropagation();
    }

    if (menu_elem) {
      form = menu_elem.querySelector('form');
      if (form) {
        inputs.pattern = form.querySelector('input[name="pattern"]');
        inputs.pattern_type = form.querySelector('select[name="pattern_type"]');
      }
    }

    params.pattern = inputs.pattern.value;
    params.pattern_type = inputs.pattern_type.value;

    search(params);
  }

  function build_output_elem(params, result) {
    console.log('build output elem');

    var button, textarea;

    if (output_elem) {
      remove_output();
    }

    output_elem = document.createElement('div');

    output_elem.innerHTML = (
      '<div><textarea cols="50" rows="10"></textarea></div>'+
      '<button class="'+button_classes+' new_search">Search again</button>'+
      '<button class="'+button_classes+' close">Close</button>'
    );
    output_elem.style = div_style;

    console.log('filling out textarea');

    textarea = output_elem.querySelector('textarea');

    textarea.value = result;

    console.log('setting up buttons');

    button = output_elem.querySelector('button.close');
    button.addEventListener('click', remove_output);

    button = output_elem.querySelector('button.new_search');
    button.addEventListener('click', function() {
      remove_output();
      build_menu(params);
    });

    document.body.appendChild(output_elem);
  }

  function build_menu(params) {
    console.log('build menu');

    var form;

    if (menu_elem) {
      remove_menu();
    }

    menu_elem = document.createElement('div');

    menu_elem.innerHTML = (
      '<form>'+
      '<div>Pattern: <input type="text" name="pattern" placeholder="optional" /></div>'+
      '<div><select name="pattern_type">'+
        '<option value="substr" selected>Substring</option>'+
        '<option value="regex">Regular expression</option>'+
      '</select></div>'+
      '</form>'+
      '<div><button class="'+button_classes+' submit">Search</button>'+
        '<button class="'+button_classes+' cancel">Cancel</button></div>'
    );
    menu_elem.style = div_style;

    console.log('setting up event listeners');

    form = menu_elem.querySelector('form');
    form.addEventListener('submit', handle_submit);

    menu_elem.querySelector('button.submit').addEventListener(
      'click', handle_submit);

    menu_elem.querySelector('button.cancel').addEventListener(
      'click', remove_menu);

    // reinsert previous values, if available
    if (params) {
      if (params.pattern) {
        form.querySelector('input[name="pattern"]').value = params.pattern;
      }

      if (params.pattern_type) {
        form.querySelector(
          'select[name="pattern_type"]').value = params.pattern_type;
      }
    }

    console.log('appending element');

    document.body.appendChild(menu_elem);
  }

  function keypress_handler(event) {
    //console.log(
    //  'got keypress: which='+event.which+' char='+event.char+' key='+event.key+
    //  ' shiftkey='+event.shiftKey+' ctrl='+event.ctrlKey);

    if (event.target && (
        event.target.tagName === 'INPUT' ||
        event.target.tagName === 'TEXTAREA')) {
      return;
    }

    // shortcut: unmodified 'd' outside of an input
    if (event.ctrlKey || event.altKey || event.shiftKey || event.metaKey ||
        !(event.key === 'd' || event.which === 68)) {
      return;
    }

    console.log('got shortcut');

    event.preventDefault();
    event.stopPropagation();

    if (!menu_elem) {
      // remove output element if present
      if (output_elem) {
        remove_output();
      }

      build_menu();
    }
    else {
      remove_menu();
    }
  }

  document.body.addEventListener('keypress', keypress_handler);

  console.log('ready to search');

}());
