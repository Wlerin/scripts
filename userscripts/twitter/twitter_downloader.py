#! /usr/bin/env python3
'''Download twitter pics saving to directories named according to the tweet's
text.'''

import sys
import re
from os.path import join, basename, exists, realpath
from os import mkdir, unlink
from subprocess import check_call, check_output, CalledProcessError
import csv


max_path_len = 260

user_agent = 'Mozilla/5.0 (X11; Linux x86_64; rv:64.0) Gecko/20100101 Firefox/64.0'

filename_regex = re.compile('Content-Disposition: inline; filename="([^"]+)"')
bad_char_regex = re.compile(r'[\\/:*?"<>|]')

def download_file(url, file_path):
    '''Download one file.'''

    if exists(file_path):
        # skip already downloaded file
        return True

    cmd_line = [
        'curl',
        '--silent',
        '-A', user_agent,
        url,
        '-o', file_path,
    ]

    try:
        check_call(cmd_line)
    except CalledProcessError:
        print('\nDownload failed for: {}'.format(url))

        # clean up possible partial file
        if exists(file_path):
            unlink(file_path)

        return False
    return True

def get_tistory_filename(url):
    '''Request a tistory file's headers and extract the filename.'''

    cmd_line = [
        'curl',
        '--silent',
        '--stderr', '-',
        # follow redirects, print headers
        '--location', '--head',
        url,
    ]

    output = check_output(cmd_line, encoding='utf-8')

    match = filename_regex.search(output)
    if not match:
        return '{}.jpg'.format(basename(url))

    return bad_char_regex.sub('_', match.group(1))

def download_from_csv(csv_path):
    '''Download twitter pics saving to directories named according to the
    tweet's text.'''

    with open(csv_path, newline='') as f:
        reader = csv.reader(f, delimiter=';')
        rows = [row for row in reader]

    i = 0
    total = len(rows)
    for row in rows:
        url, username, dir_name = row[:3]

        # make sure dir names are not empty
        username = username if username else 'No username'
        dir_name = dir_name if dir_name else 'Empty tweet'

        filename = (get_tistory_filename(url) if 'tistory' in url
                    else basename(url).replace(':orig', ''))

        # truncate dir name if necessary to make the whole path fit windows's
        # limit
        path_len = len(join(realpath('.'), username, dir_name, filename))
        if path_len > max_path_len:
            dir_name = dir_name[:-(path_len-max_path_len)]

        # create dirs if necessary
        if not exists(username):
            mkdir(username)

        dir_path = join(username, dir_name)
        if not exists(dir_path):
            mkdir(dir_path)

        file_path = join(dir_path, filename)

        i += 1
        print('\rdownloading: {}/{}'.format(i, total), end='', file=sys.stderr)

        if not download_file(url, file_path):
            # download failed, abort
            return
    print(file=sys.stderr)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        exit('Usage: {} links.csv'.format(sys.argv[0]))

    download_from_csv(sys.argv[1])
