// ==UserScript==
// @name            Selca - add media link to original
// @namespace       https://gitlab.com/loopvid/scripts
// @description     Add link to media page under the /original/ image
// @include         https://selca.kastden.org/original/*
// @version         3.1
// @grant           none
// @run-at          document-start
// ==/UserScript==

(function() {
  'use strict';

  var enable_shortcuts = true,
      original_regex = new RegExp('(.*)/original/(\\d+)/?'),
      requesting = false,
      shortcuts;

  shortcuts = [
    {key: 'm', keyCode: 77, link_id: 'media_link'},
    {key: 'p', keyCode: 80, link_id: 'post_link'},
    {key: 'u', keyCode: 85, link_id: 'user_link'},
    {key: 'f', keyCode: 70, link_id: 'favorite_link'},
    {key: 'r', keyCode: 82, link_id: 'noona_link'},
  ];

  function send_request(target, values, on_success, on_error) {
    var request, data, key;

    if (requesting) { return; }
    requesting = true;

    // build request
    request = new window.XMLHttpRequest();
    request.open('POST', target, true);

    request.onload = function() {
      var data;

      requesting = false;

      if (request.status >= 200 && request.status < 400) {
        data = JSON.parse(request.responseText);
        on_success(data);
      }
      else {
        on_error();
      }
    };

    request.onerror = function() {
      requesting = false;
      on_error();
    };

    // build data
    data = new window.FormData();
    if (values) {
      for (key in values) {
        data.append(key, values[key]);
      }
    }

    request.send(data);
  }

  function is_video() {
    /* check if this is a video */
    var videos = document.getElementsByTagName('VIDEO');

    return videos.length > 0;
  }

  function is_firefox() {
    /* check if this is (probably) firefox */
    return navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
  }

  function get_pos() {
    /* check if this is a video and if so, try to change the links' position to
     * make room for video controls at the bottom of the page */

    var footer,
        footer_height = 30;

    if (is_video()) {
      // try to extract the correct height if the footer is in place
      footer = document.getElementById('footer');
      if (footer) {
        footer_height = parseInt(footer.clientHeight);
      }
      return 'left: 5px; bottom: '+ (5+footer_height) + 'px;';
    }
    return 'left: 5px; bottom: 5px;';
  }

  function get_colors() {
    /* return the better looking set of background and foreground colors for
     * the current page */

    var darkbg = {
          div: 'background-color: rgba(0, 0, 0, 0.75); color: #fff;',
          link: 'color: #48f;'
        },
        lightbg = {
          div: 'background-color: rgba(206, 196, 196, 0.75); color: #000;',
          link: 'color: #04a;'
        };

    // for firefox, always assume a dark background
    // for chrome and others, assume dark background for video and light for
    // other stuff
    if (is_firefox() || is_video()) {
      return darkbg;
    }
    else {
      return lightbg;
    }
  }

  function get_media_id() {
    var match = original_regex.exec(window.location.href);
    if (!match) { return; }

    return match[2];
  }

  function set_up_noona_link(parent_elem, noona_type, name) {
    var link, url;

    url = '/noona/'+noona_type+'/'+name+'/';

    link = document.createElement('a');
    link.classList.add('media_link');
    link.id = 'noona_link';
    link.innerHTML = 'profile';
    link.href = url;

    parent_elem.appendChild(link);
  }

  function set_up_favorite_link(parent_elem, media_id) {
    var link;

    link = document.createElement('a');
    link.classList.add('media_link');
    link.id = 'favorite_link';
    link.innerHTML = 'favorite';
    link.href = '#';

    function on_error(message) {
      window.alert(message);
    }

    function on_success(data) {
      if (data.success) {
        parent_elem.removeChild(link);
        parent_elem.appendChild(document.createTextNode('favorited'));
      }
      else {
        on_error(data.error);
      }
    }

    link.addEventListener('click', function(event) {
      event.preventDefault();
      event.stopPropagation();

      if (requesting) { return; }

      send_request('/json/add_remove_favorite', {'media_id': media_id},
                   on_success, on_error);
    });

    parent_elem.appendChild(link);
  }

  function check_media(parent_elem, media_id) {
    function on_error(message) {
      if (message) {
        window.alert(message);
      }
    }

    function on_success(data) {
      // set up noona link
      if (data.noona && data.noona.completed) {
        set_up_noona_link(parent_elem, data.noona.type, data.noona.name);
        parent_elem.appendChild(document.createTextNode(' '));
      }

      // set up favorite link
      if (data.media && data.media.favorite_id) {
        parent_elem.appendChild(document.createTextNode('favorited'));
      }
      else if (data.selca_user && data.selca_user.selca_user_id) {
        set_up_favorite_link(parent_elem, media_id);
      }
      else {
        parent_elem.appendChild(document.createTextNode('not logged in'));
      }

      // replace redirect links
      if (data.media && data.media.url) {
        document.getElementById('external_media_link').href = data.media.url;
      }
      if (data.post && data.post.post_id) {
        document.getElementById('post_link').href = '/post/'+data.post.post_id+'/';
      }
      if (data.post && data.post.url) {
        document.getElementById('external_post_link').href = data.post.url;
      }
      if (data.owner && data.owner.name) {
        document.getElementById('user_link').href = '/owner/'+data.owner.name+'/';
      }
    }

    send_request('/json/media_info/'+media_id, null, on_success, on_error);
  }

  function click_link(link_id) {
    var link = document.getElementById(link_id),
        evt = new MouseEvent(
          'click', {bubbles: true, cancelable: true, view: window});

    if (link) {
      link.dispatchEvent(evt);
      return true;
    }
    return false;
  }

  function handle_keydown(event) {
    // only capture unmodified keys
    if (event.ctrlKey || event.altKey || event.shiftKey || event.metaKey) {
      return;
    }

    var i;

    for (i=0; i < shortcuts.length; i++) {
      if (event.key === shortcuts[i].key || event.keyCode === shortcuts[i].keyCode) {
        // only prevent default if the function returns a success
        if (click_link(shortcuts[i].link_id)) {
          event.preventDefault();
          event.stopPropagation();
        }
        break;
      }
    }
  }

  function set_up_css() {
    var sheet, elem, colors;

    // insert new sheet for our custom rules
    elem = document.createElement('style');
    document.head.appendChild(elem);
    sheet = elem.sheet;

    // insert rules
    colors = get_colors();

    sheet.insertRule('.media_link { text-decoration: none; '+colors.link+' }', 0);
    sheet.insertRule(
      '.links_div { position: fixed; display: inline-block; z-index: 1; '+
      'padding: 1px 5px; '+get_pos()+' '+colors.div+' }', 1);
  }

  function set_up_links() {
    var div,
        media_id = get_media_id();

    set_up_css();

    div = document.createElement('DIV');

    div.classList.add('links_div');

    // create static links
    div.innerHTML = (
      '<a class="media_link" id="media_link" href="/media/'+media_id+'/">media</a> '+
      '<a class="media_link" id="external_media_link" rel="noreferrer" href="/redirect/media/external/media/'+media_id+'/">(external)</a> '+
      '<a class="media_link" id="post_link" href="/redirect/media/post/'+media_id+'/">post</a> '+
      '<a class="media_link" id="external_post_link" rel="noreferrer" href="/redirect/media/external/post/'+media_id+'/">(external)</a> '+
      '<a class="media_link" id="user_link" href="/redirect/media/owner/'+media_id+'/">user</a> '
    );

    document.body.appendChild(div);

    check_media(div, media_id);
  }

  function set_up() {
    set_up_links();

    if (enable_shortcuts) {
      document.addEventListener('keydown', handle_keydown);
    }
  }

  /* images and videos don't fire DOMContentLoaded or load like regular pages,
   * and we want to allow some time for video_controls.user.js to run if it is
   * available */
  window.setTimeout(set_up, 20);

}());
