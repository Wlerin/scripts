// ==UserScript==
// @name          Add hidden links to images' originals
// @namespace     https://gitlab.com/loopvid/scripts
// @description   Add hidden links to images' originals
// @include       http*://*.blog.me/*
// @include       http*://*.naver.com/*
// @include       http*://*blog.daum.net/*
// @include       http*://*cafe.daum.net/*
// @include       http*://entertain.v.daum.net/v/*
// @include       http*://*tistory.com/*
// @exclude       http*://cfile*.uf.tistory.com/*
// @include       http*://*all-idol.com/*
// @include       http*://*hvstudio.net/*
// @include       http*://*idol-grapher.com/*
// @include       http*://*idoleemo.com/*
// @include       http*://*9mworld.com/*
// @include       http*://*thestudio.kr/*
// @include       http*://*apinkstudio.com/*
// @include       http*://*drighk.com/*
// @include       http*://*harane.com/*
// @include       http*://*karagrapher.com/*
// @include       http*://*junhyoseong.com/*
// @include       http*://www.eunjibee.com/*
// @include       http*://*cp-studio.net/*
// @include       http*://*bangminah.com/*
// @include       http*://ohmygirl.kr/*
// @include       http*://dooooly.com/*
// @include       http*://plus.google.com/*
// @include       http*://*.blogspot.*/*
// @include       http*://twitter.com/*
// @include       http*://mobile.twitter.com/*
// @exclude       http*://twitter.com/i/videos/*
// @include       http*://addyk.co.kr/wordpress/*
// @include       http*://news1.kr/*
// @include       http*://www.topstarnews.net/*
// @include       http*://tenasia.hankyung.com/*
// @include       http*://www.instagram.com/*
// @include       http*://photobank.kbs.co.kr/*
// @include       http*://program.sbs.co.kr/*
// @include       http*://star.ohmynews.com/*
// @include       http*://gifyu.com/*
// @exclude       http*://gifyu.com/images/*
// @include       http*://www.weibo.com/*
// @include       http*://channels.vlive.tv/*/celeb/*
// @include       http*://osen.co.kr/*
// @version       2.33
// @grant         none
// @run-at        document-start
// ==/UserScript==

(function() {
  'use strict';

  var services, custom_handlers,
      debug = false,
      waiting_for_mutations = false;

  services = [
    // vlive (must come before naver)
    {
      'url_regex': new RegExp(
        'https?://vfan-phinf\\.(naver|pstatic)\\.net/.*\\.jpe?g(\\?type=.+)?$', 'i'),
      'subs': [
        [/(\?[^?]+)?$/, '?attachment']
      ]
    },
    // naver blog, naver post, naver entertainment
    {
      'url_regex': new RegExp(
        'https?://[^/]+\\.(naver|pstatic)\\.net/[^"\']*\\.(jpe?g|gif|png)', 'i'),
      'subs': [
        [/\/\/(postfiles[^.]*|[^.]*blogthumb-phinf)\./, '//blogfiles.'],
        [/\?[^?]+$/, '']
      ]
    },
    // old tistory
    {
      'url_regex': new RegExp(
        'https?://(.+\\.daumcdn\\.net/thumb/.*)?'+
          'cfile(\\d+)\\.uf\\.(tistory\\.com|daum\\.net)'+
          '.+([0-9A-Z]{22})$', 'i'),
      'subs': [
        [
          /https?:\/\/(.+\.daumcdn\.net\/thumb\/.*)?cfile(\d+)\.uf\.(tistory\.com|daum\.net).+([0-9A-Z]{22})$/i,
          'http://cfile$2.uf.$3/original/$4'
        ]
      ],
      'unquote': true
    },
    // new tistory
    {
      'url_regex': new RegExp(
        'https?://t\\d+\\.daumcdn\\.net/cfile/tistory/[0-9A-Z]{18}$', 'i'),
      'subs': [
        [/(\?.*)?$/, '?original']
      ]
    },
    // daumcdn thumbs
    {
      'url_regex': new RegExp(
        'https?://img\\d+\\.daumcdn\\.net/thumb/.*\\?.*fname=https?.*\\.jpg.*$', 'i'),
      'subs': [
        [/https?:\/\/img\d+\.daumcdn\.net\/thumb\/.*\?.*fname=/i, ''],
        [/\.jpg.+$/i, '.jpg']
      ],
      'unquote': true
    },
    // googleplus, blogspot
    {
      'url_regex': new RegExp(
        'https?://(lh.*\\.googleusercontent\\.com|.*\\.bp\\.blogspot\\.com)/'+
          '.*/[swh]\\d+(-[a-z][a-z0-9=,-]*)?/[^/]+$', 'i'),
      'subs': [
        [/\/[swh]\d+(-[a-z][a-z0-9=,-]*)?\/([^\/]+)$/, '/s0/$2']
      ]
    },
    // twitter
    {
      'url_regex': new RegExp(
        'https?://pbs\\.twimg\\.com/media/.*\\.jpg(:[a-z]+)?$', 'i'),
      'subs': [
        [/\.jpg(:[a-z]+)?$/, '.jpg:orig']
      ]
    },
    {
      'url_regex': new RegExp(
        'https?://pbs\\.twimg\\.com/media/[^./"\']+\\?[^"\']*format=jpg', 'i'),
      'subs': [
        [/\?.*/, '.jpg:orig']
      ]
    },
    // addyk
    {
      'url_regex': new RegExp(
        'https?://addyk\\.co\\.kr/wordpress/wp-content/uploads/.*-\\d+x\\d+\\.jpg$', 'i'),
      'subs': [
        [/-\d+x\d+\.jpg$/, '.jpg']
      ]
    },
    // news1
    {
      'url_regex': new RegExp(
        '^https?://image\\.news1\\.kr/.*/(article|no_water)\\.jpg$', 'i'),
      'subs': [
        [/(article|no_water)\.jpg$/, 'original.jpg'],
        [/\/\/system/, '/system']
      ]
    },
    // topstarnews
    {
      'url_regex': new RegExp(
        '^https?://(main|uhd)\\.img\\.topstarnews\\.net/.*/file_attach(_thumb)?/'+
        '.+/[0-9-]+(_.*|-org)?\\.jpg$', 'i'),
      'subs': [
        [/main\.img\.topstarnews/, 'uhd.img.topstarnews'],
        [/\/file_attach_thumb\//, '/file_attach/'],
        [/([0-9-]+)(_.*)?\.jpg$/, '$1-org.jpg']
      ],
      'alt_src': function(image) {
        if (image && image.dataset) {
          return image.dataset.src;
        }
      }
    },
    {
      'url_regex': new RegExp(
        '^https?://www\\.topstarnews\\.net/news/thumbnail/.*_v\\d+\\.jpg$', 'i'),
      'subs': [
        [/\/thumbnail\//, '/photo/'],
        [/_v\d+\.jpg$/, '_org.jpg']
      ]
    },
    {
      'url_regex': new RegExp(
        '^https?://www\\.topstarnews\\.net/news/photo/.*\\d+\\.jpg$', 'i'),
      'subs': [
        [/\.jpg$/, '_org.jpg']
      ]
    },
    // tenasia
    {
      'url_regex': new RegExp(
        '^https?://(img\\.)?tenasia\\.hankyung\\.com/.*-\\d+x\\d+\\.jpg$', 'i'),
      'subs': [
        [/-\d+x\d+\.jpg$/, '.jpg']
      ]
    },
    // kbs photobank
    {
      'url_regex': new RegExp(
        '^https?://[^/]+/PHOTOBANK/(small|mid)_image/[A-Z]\\d+/.+\\.[^.]+$'),
      'subs': [
        [/\/(small|mid)_image\//, '/origin_image/']
      ]
    },
    // sbs program
    {
      'url_regex': new RegExp(
        '^https?://img\\d+\\.sbs\\.co\\.kr/img/sbs/.+/[^/]+_w\\d+_h\\d+.jpg$', 'i'),
      'subs': [
        [/_w\d+_h\d+.jpg$/, '_ori.jpg']
      ]
    },
    // oh my news
    {
      'url_regex': new RegExp(
        '^https?://ojsfile\\.ohmynews\\.com/PHT_IMG_FILE/.+_PHT\\.jpg$', 'i'),
      'subs': [
        [/PHT_IMG_FILE/, 'BIG_IMG_FILE'],
        [/_PHT\.jpg/, '_BIG.jpg']
      ],
      'alt_src': function(image) {
        if (image && image.dataset) {
          return image.dataset.phtSrc;
        }
      }
    },
    // gifyu
    {
      'url_regex': new RegExp(
        '^https?://([^/]+\\.)?gifyu\\.com/images/.+\\.md\\.[^.]+$', 'i'),
      'subs': [
        [/\.md(\.[^.]+)$/, '$1']
      ]
    },
    // weibo
    {
      'url_regex': new RegExp(
        '^https?://([^/]+\\.)?sinaimg\\.cn/mw\\d+/.+$', 'i'),
      'subs': [
        [/[/]mw\d+[/]/, '/mw2048/']
      ]
    },
    // osen
    {
      'url_regex': new RegExp(
        '^https?://file\\.osen\\.co\\.kr/article(_thumb)?/\\d.*\\.jpg', 'i'),
      'subs': [
        [/[/]article(_thumb)?[/]/, '/article/original/'],
        [/_\d+x\d*\.jpg/, '.jpg']
      ]
    }
  ];

  function build_original_url(url, substitutions) {
    var i, regex, replacement;

    // make all substitutions applicable
    for (i=0; i < substitutions.length; i++) {
      regex = substitutions[i][0];
      replacement = substitutions[i][1];
      url = url.replace(regex, replacement);
    }

    return url;
  }

  function get_original_url_from_element(element) {
    var i, alt_url, url, original_url;

    for (i=0; i < services.length; i++) {

      url = element.src;

      if (services[i].alt_src) {
        // use alternative source for the image url when available
        alt_url = services[i].alt_src(element);
        if (alt_url) {
          //console.log('found alternative source '+alt_url);
          url = alt_url;
        }
      }

      //console.log(url);
      if (!url) {
        // nothing to test
        continue;
      }

      if (services[i].url_regex.test(url)) {
        //console.log('got match for '+url);
        original_url = build_original_url(url, services[i].subs);
        return services[i].unquote ? unescape(original_url) : original_url;
      }
    }

    return null;
  }

  function add_hidden_link(url) {
    var link;

    //console.log('adding link for '+url);

    if (document.querySelector('a[href="'+url+'"]')) {
      // link for this url has already been added
      return false;
    }

    link = document.createElement('A');
    link.href = url;
    link.style = 'display: none;';
    document.body.appendChild(link);

    return true;
  }

  function default_extractor(custom_selector) {
    /* go through all unhandled images and extract their urls */

    var i, original_url, images,
        urls = [],
        selector = 'img';

    if (custom_selector) {
      selector = custom_selector;
    }
    images = document.querySelectorAll(
      selector+':not([data-original_handled="true"])');

    console.log('found '+images.length+' images');
    if (images.length <= 0) { return urls; }

    for (i=0; i < images.length; i++) {
      original_url = get_original_url_from_element(images[i]);

      if (original_url) {
        urls.push(original_url);
      }

      images[i].dataset.original_handled = 'true';
    }

    return urls;
  }

  function json_extractor(text, url_regex, subs, group) {
    /* extract urls from json */

    var match, original_url,
        urls = [];

    // go through all matches in the text
    while (true) {
      match = url_regex.exec(text);
      if (!(match && match[0])) { break; }

      original_url = build_original_url(match[group], subs);

      if (original_url) {
        //console.log("found url '"+match[0]+"' -> "+original_url);
        urls.push(original_url);
      }
    }

    return urls;
  }

  function set_up_links(original_urls) {
    var i,
        added = 0;

    if (!original_urls) { return; }

    // add hidden links
    for (i=0; i < original_urls.length; i++) {
      if (add_hidden_link(original_urls[i])) {
        if (debug) {
          console.log(original_urls[i]);
        }
        added += 1;
      }
    }

    console.log('added links for '+added+' out of '+original_urls.length+
                ' urls');
  }

  function set_up_mutation_observer(extractor) {
    var observer = new MutationObserver(function() {
      // make sure we don't run too often while the page is changing
      if (waiting_for_mutations) { return; }

      //console.log('got mutation');

      waiting_for_mutations = true;

      window.setTimeout(function() {
        // start observing again immediately so we don't miss mutations that
        // happen while we're setting up links
        waiting_for_mutations = false;

        set_up_links(extractor());
      }, 500);
    });

    observer.observe(document.body, {childList: true, subtree: true});
  }

  function overload_xmlhttprequest(extractor) {
    /* Overload XMLHttpRequest's constructor so we can add our own listener for
     * the load event and extract urls from the response.
     *
     * WARNING: @grant GM_xmlhttpRequest breaks this function. */

    var _XMLHttpRequest = window.XMLHttpRequest;

    window.XMLHttpRequest = new Proxy(_XMLHttpRequest, {
      construct: function(target, argumentsList, newTarget) {
        /* jshint unused:false, newcap:false */

        //console.log('new XMLHttpRequest');
        var req = new _XMLHttpRequest();

        req.addEventListener('load', function() {
          //console.log('got new response');

          // look for new urls in the response
          set_up_links(extractor(req.responseText));
        });

        return req;
      }
    });
  }

  function default_init(extractor) {
    // observe page for new elements so we can extract again
    set_up_mutation_observer(extractor);
    // extract urls from the initial elements
    set_up_links(extractor());
  }

  function instagram_init() {
    var extractor,
        url_regex = new RegExp(
          '"(display|video)_(src|url)": *"'+
          '(https?://[^/]+\\.(cdninstagram\\.com|fbcdn\\.net)/'+
          '[^"\':;,]*\\.(jpg|mp4))"',
          'gi'),
        subs = [];

    extractor = function(text) {
      return json_extractor(text, url_regex, subs, 3);
    };

    // monitor requests for new urls
    overload_xmlhttprequest(extractor);

    // extract initial urls contained in the page
    set_up_links(extractor(JSON.stringify(window._sharedData)));
  }

  function naver_entertain_init() {
    var extractor, i, scripts,
        url_regex = new RegExp(
          'https?://[^/]+\\.(naver|pstatic)\\.net/[^"\':;,]*\\.jpe?g', 'gi'),
        subs = [];

    extractor = function(text) {
      return json_extractor(text, url_regex, subs, 0);
    };

    // monitor requests for new urls
    overload_xmlhttprequest(extractor);

    // extract initial urls contained in the page, when available
    scripts = document.getElementsByTagName('script');
    for (i=0; i < scripts.length; i++) {
      if (scripts[i].innerHTML.indexOf('photoListJSON') > -1) {
        set_up_links(extractor(scripts[i].innerHTML));
        break;
      }
    }
  }

  function naver_post_init() {
    var extractor, i, scripts,
        url_regex = new RegExp(
          'https?://[^./"\']+\\.(naver|pstatic)\\.net/[^"\']*\\.(jpe?g|gif|png)', 'gi'),
        subs = [];

    extractor = function(text) {
      return json_extractor(text, url_regex, subs, 0);
    };

    // extract urls from html contained inside script tags
    scripts = document.getElementsByTagName('script');
    for (i=0; i < scripts.length; i++) {
      if (scripts[i].innerHTML.indexOf('<img') > -1) {
        set_up_links(extractor(scripts[i].innerHTML));
      }
    }
  }

  custom_handlers = [
    // instagram
    {
      'page_regex': new RegExp('^https?://www\\.?instagram\\.com'),
      'init': instagram_init,
      'extractor': null
    },
    // naver entertainment
    {
      'page_regex': new RegExp(
        '^https?://(m\\.)?entertain\\.?naver\\.com/(topic|photo|entertain)'),
      'init': naver_entertain_init,
      'extractor': null
    },
    // naver post
    {
      'page_regex': new RegExp('^https?://(m\\.)?post\\.?naver\\.com/'),
      'init': naver_post_init,
      'extractor': null
    },
    // oh my news
    {
      'page_regex': new RegExp('^https?://star\\.ohmynews\\.com/'),
      'extractor': function() {
        // extract from thumbnail divs instead of regular images
        return default_extractor('div.cssThumb[data-pht-src*="PHT"]');
      }
    }
  ];

  function initial_set_up() {
    var i, handler,
        extractor = default_extractor,
        init = default_init;

    // check if page uses custom handlers
    for (i=0; i < custom_handlers.length; i++) {
      handler = custom_handlers[i];

      if (handler.page_regex.test(window.location.href)) {

        if (handler.init) {
          console.log('using custom init');
          init = handler.init;
        }

        if (handler.extractor) {
          console.log('using custom extractor');
          extractor = handler.extractor;
        }

        break;
      }
    }

    init(extractor);
  }

  document.addEventListener('DOMContentLoaded', initial_set_up);

}());
