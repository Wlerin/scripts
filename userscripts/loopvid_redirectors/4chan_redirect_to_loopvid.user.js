// ==UserScript==
// @name          4chan - redirect webms to loopvid
// @namespace     https://gitlab.com/loopvid/scripts
// @description   Redirect 4chan webms to loopvid
// @include       https://i.4cdn.org/*/*.webm
// @include       http://i.4cdn.org/*/*.webm
// @version       1.0
// @grant         none
// @run-at        document-start
// ==/UserScript==

(function() {
  'use strict';

  var new_url, path = window.location.pathname;

  function stop_video(video) {
    video.pause();
    // remove sources
    while (video.firstChild) {
      video.removeChild(video.firstChild);
    }
    // remove video
    video.parentNode.removeChild(video);
  }

  // get rid of video so it won't load twice
  var videos = document.getElementsByTagName('video');
  if (videos.length > 0) {
    stop_video(videos[0]);
  }

  new_url = 'https://loopvid.appspot.com/#fc' + path.replace('.webm', '');
  window.location.replace(new_url);

}());
