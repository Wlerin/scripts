// ==UserScript==
// @name        imgur - redirect gif to gifv
// @namespace   https://gitlab.com/loopvid/scripts
// @include     https://i.imgur.com/*.gif
// @include     http://i.imgur.com/*.gif
// @version     1.0
// @grant       none
// @run-at      document-start
// ==/UserScript==

(function() {
  'use strict';

  var url = window.location.href;

  // make sure we're at the right place
  if (!/\.gif$/.test(url)) { return; }

  // get rid of image so it won't load twice
  var images = document.getElementsByTagName('img');
  if (images.length > 0) {
    images[0].parentNode.removeChild(images[0]);
  }

  // redirect
  window.location.replace(url+'v');

}());
