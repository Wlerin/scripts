// ==UserScript==
// @name          Instagram image to original
// @namespace     https://gitlab.com/loopvid/scripts
// @description   Redirects instagram images to the original resolution.
// @include       https://*.cdninstagram.com/*.jpg*
// @include       https://*.fbcdn.net/*.jpg*
// @version       1.1
// @grant         none
// @run-at        document-start
// ==/UserScript==

/* WARNING: This script can no longer do its job since instagram introduced
 * mandatory url signatures. Using it will result in access denied errors. */

(function() {
  'use strict';

  /* disabling script; see warning above */
  return;

  function remove_image() {
    /* get rid of image so it won't load twice */
    var images = document.getElementsByTagName('img');
    if (images.length > 0) {
      images[0].parentNode.removeChild(images[0]);
    }
  }

  function redirect() {
    /* redirect to original image */
    var i,
        url = window.location.href,
        to_remove = [
          // downscaling
          /\/(s|p)\d+x\d+\//,
          // cropping
          /\/c\d(\.\d+){3}\//,
          // sharpening
          /\/sh\d+(\.\d+)?\//
        ];

    // remove all portions of the url that change the image
    for (i=0; i<to_remove.length; i++) {
      url = url.replace(to_remove[i], '/');
    }

    // check that changes were made
    if (url === window.location.href) {
      console.log('already at original image');
      return;
    }

    remove_image();

    // redirect
    window.location.replace(url);
  }

  // run immediately
  redirect();

}());
