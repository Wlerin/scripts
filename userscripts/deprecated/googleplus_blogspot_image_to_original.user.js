// ==UserScript==
// @name          Google+, blogspot - image to original
// @namespace     https://gitlab.com/loopvid/scripts
// @description   Redirects resized googleplus/blogspot image to the original.
// @include       https://lh*.googleusercontent.com/*
// @include       https://*.bp.blogspot.com/*
// @version       0.9.1
// @grant         none
// @run-at        document-start
// ==/UserScript==

(function() {
  'use strict';

  var path = window.location.pathname,
      split_path = path.split('/');

  function redirect(new_url) {
    // get rid of image so it won't load twice
    var images = document.getElementsByTagName('img');
    if (images.length > 0) {
      images[0].parentNode.removeChild(images[0]);
    }

    // make sure we don't loop infinitely
    if (new_url !== window.location.href) {
      // redirect
      window.location.replace(new_url);
    }
  }

  function check_url() {
    var resize_info;
    
    // check the last dir in path for resizing info
    resize_info = split_path[split_path.length-2];
    if (/[swh]\d+(-[a-z][a-z0-9=,-]*)?/.test(resize_info) &&
        resize_info !== 's0') {
      return true;
    }
    return false;
  }

  function check_for_image() {
    /* make sure the current document is an image, otherwise don't run */

    // check that we're not inside a frame
    try { if (window.self !== window.top) { return false; } }
    catch (e) { return false; }

    if (document.contentType !== undefined) {
      // for firefox only, check document.contentType
      if (/^image\//.test(document.contentType)) { return true; }
    }
    else {
      // for non-firefox, check that the document consists of a single image

      /* note that we're assuming the DOM will already be in place at
       * document-start, which currently works for chrome even though it might
       * not work in future versions or other browsers. */
      if (document.body.childNodes.length === 1 &&
          document.body.childNodes[0].tagName === 'IMG') {
        return true;
      }
    }
    return false;
  }

  function get_new_url() {
    var new_path;

    // replace resize info with noop
    split_path[split_path.length-2] = 's0';
    new_path = split_path.join('/');
    return window.location.href.replace(path, new_path);
  }

  if (check_url() && check_for_image()) {
    redirect(get_new_url());
  }

}());
