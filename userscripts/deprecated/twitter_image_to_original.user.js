// ==UserScript==
// @name          Twitter - image to original
// @namespace     https://gitlab.com/loopvid/scripts
// @description   Redirects twitter images of smaller size to :orig.
// @include       https://pbs.twimg.com/media/*
// @include       http://pbs.twimg.com/media/*
// @version       1.3
// @grant         none
// @run-at        document-start
// ==/UserScript==

(function() {
  'use strict';

  var url = window.location.href,
      size_regex = new RegExp('\\.jpg(:[a-z]+)?$'),
      orig_regex = new RegExp(':orig$');

  // only redirect if not orig already
  if (size_regex.test(url) && !orig_regex.test(url)) {
    // get rid of image so it won't load twice
    var images = document.getElementsByTagName('img');
    if (images.length > 0) {
      images[0].parentNode.removeChild(images[0]);
    }

    // replace any :* at the end, if present, with :orig
    var new_url = url.replace(size_regex, '.jpg:orig');

    // make sure we don't loop infinitely
    if (new_url !== url) {
      // redirect
      window.location.replace(new_url);
    }
  }

}());
