// ==UserScript==
// @name        gfycat open mp4 directly
// @namespace   https://gitlab.com/loopvid/scripts
// @include     https://gfycat.com/*
// @include     http://gfycat.com/*
// @include     https://www.gfycat.com/*
// @include     http://www.gfycat.com/*
// @version     1.0
// @grant       none
// @run-at      document-start
// ==/UserScript==

(function() {
  'use strict';

  var interval,
    path = window.location.pathname;

  /* check if actual video page (that is, not their front page, the video
   * itself, etc) */
  if (!/^\/([A-Z][a-z]+){3}$/.test(path)) { return; }

  function stop_video() {
    var video = document.querySelector('video');

    if (video) {
      video.pause();
      // remove sources
      while (video.firstChild) {
        video.removeChild(video.firstChild);
      }
      // remove video
      video.parentNode.removeChild(video);
    }
  }

  function check_source() {
    var source = document.getElementById('mp4Source');

    if (source && source.src) {
      window.clearInterval(interval);

      console.log('redirecting to source');
      stop_video();
      window.location.replace(source.src);
    }
  }

  // keep checking until the source becomes available
  interval = window.setInterval(check_source, 20);

  // try checking immediately
  check_source();

  window.addEventListener('load', function() {
    // try running one more time; if the source isn't available yet, then the
    // page doesn't have one
    window.clearInterval(interval);
    check_source();
  });

}());
