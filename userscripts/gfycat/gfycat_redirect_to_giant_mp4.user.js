// ==UserScript==
// @name        gfycat - redirect to mp4
// @namespace   https://gitlab.com/loopvid/scripts
// @include     https://gfycat.com/*
// @include     http://gfycat.com/*
// @include     https://www.gfycat.com/*
// @include     http://www.gfycat.com/*
// @version     1.0
// @grant       none
// @run-at      document-start
// ==/UserScript==

(function() {
  'use strict';

  var extension = '.mp4',
    path = window.location.pathname,
    source_url = 'https://giant.gfycat.com'+path+extension;

  /* check if actual video page (that is, not their front page, the video
   * itself, etc) */
  if (!/^\/([A-Z][a-z]+){3}$/.test(path)) { return; }

  function stop_video() {
    var video = document.querySelector('video');

    if (video) {
      video.pause();
      // remove sources
      while (video.firstChild) {
        video.removeChild(video.firstChild);
      }
      // remove video
      video.parentNode.removeChild(video);
    }
  }

  stop_video();
  window.location.replace(source_url);

}());
