#! /usr/bin/env python3
'''Rename files containing url-quoted characters'''

import urllib.request
import sys
import re
import os
import os.path

bad_char_regex = re.compile(r'[\\/:*?"<>|]')

def unquote_file(file, quiet=False):
    '''Rename a file, if it contains url-quoted characters, otherwise do
    nothing.'''

    dirname, base = os.path.split(file)
    destination_base = urllib.request.unquote(base)

    # check if anything got unquoted before replacing bad chars to make sure we
    # don't rename unquoted files containing bad chars
    if destination_base == base:
        print('Nothing to be done:', file)
        return

    # remove bad chars
    destination_base = bad_char_regex.sub('_', destination_base)

    if dirname:
        destination = os.path.join(dirname, destination_base)
    else:
        destination = destination_base

    if os.path.exists(destination):
        if not quiet:
            print('Destination exists; skipping:', destination)
        return
    elif not os.path.exists(file):
        if not quiet:
            print("File doesn't exist:", file)
        return
    else:
        os.rename(file, destination)
        if not quiet:
            print('‘{}’ -> ‘{}’'.format(file, destination))


if __name__ == '__main__':
    if len(sys.argv) < 2:
        exit('Usage: {} *%*.*'.format(sys.argv[0]))

    for file in sys.argv[1:]:
        unquote_file(file)
